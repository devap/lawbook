# ISKCON Lawbook


Installation of Dependencies
============================

First, make sure you are using virtualenv (http://www.virtualenv.org)

The following will set the correct environment variables and modify your prompt 
while the virtualenv is activated:

    $ cat >>$HOME/.virtualenvs/postactivate <<'EOF'
    export _OLD_PROMPT="$PROMPT"
    export _OLD_RPROMPT="$RPROMPT"
    PROMPT="%F{white}(`basename \"$VIRTUAL_ENV\"`)%f $PROMPT"
    RPROMPT="%F{white}(%f%F{green}`basename \"$VIRTUAL_ENV\"`%f%F{white})%f $RPROMPT"

    export DJANGO_SETTINGS_MODULE=config.settings.local
    EOF

This will restore the prompt and django settings when deactivating the virtualenv:

    $ cat >>$HOME/.virtualenvs/postdeactivate <<'EOF'
    PROMPT="$_OLD_PROMPT"
    RPROMPT="$_OLD_RPROMPT"
    cd ~
    unset DJANGO_SETTINGS_MODULE
    EOF

Then create a virtualenv and tell it to add this project's root location using the '-a' flag:

Create the virtualenv:

    $ mkvirtualenv --python=python3 <virtualenv_name> -a <project_directory>

From the parent directory:

    $ mkvirtualenv --python=python3 lawbook -a iskcon_lawbook

From within the project directory:

    $ mkvirtualenv --python=python3 lawbook -a .

To check if you have Node.js installed, run this command in your terminal:
(https://nodejs.org/en/)

    $ node -v

To confirm that you have npm installed you can run this command in your terminal:
(https://www.npmjs.com/get-npm)

    $ npm -v

To get the latest version of npm:

    $ npm install npm@latest -g

Then, depending on where you are installing dependencies:
    (since the virtualenv was created using python3, pip will automatically invoke pip3)

In development::

    $ pip install -r requirements/local.txt
    $ npm install

For production::

    $ pip install -r requirements/prod.txt
    $ npm install

*note: We install production requirements this way because many Platforms as a Services expect a requirements.txt file in the root of projects.*

After installing the pip dependencies, you'll need to reactivate the virtualenv for all the libraries to take effect:

    $ deactivate
    $ workon lawbook

The config files need to be symlinked into place for both uwsgi and django:

Local development:

    $ cd ./config
    $ ln -s wsgi-local.py wsgi.py
    $ ln -s /etc/environments/local/lawbook.ini uwsgi.ini

Production environment:

    $ cd ./config
    $ ln -s wsgi-prod.py wsgi.py
    $ ln -s /etc/environments/production/lawbook.ini /etc/uwsgi/apps-available/lawbook.ini
    $ ln -s /etc/uwsgi/apps-available/lawbook.ini /etc/uwsgi/apps-enabled/lawbook.ini

Install Postgresql:

    $ brew install postgresql
    $ brew services start postgresql

Create the local postgres user:

    $ createuser -d -P lawbook

Create the local database:

    $ createdb -O lawbook -T template0 iskcon_lawbook_dev

Run django migrate command to create the database tables:

    $ django-admin migrate

Import groups

    $ django-admin loaddata ./data/groups.json

Create a django admin superuser:

    $ django-admin shell_plus
    >>> from django.contrib.auth.models import User
    >>> user = User.objects.create_superuser('name', 'name@email.com', 'password')
    >>> user.save()
    >>> [ctrl-d to exit shell]

Create tastypie api keys for superuser

    $ django-admin backfill_api_keys

Add superuser to Admin and Developer groups (for testing apis)

    $ django-admin loaddata ./data/user_groups.json

There are several fixture files to pre-populate the datebase with certain default values:
(this script calls django-admin.py and passes it various fixture files located in ./data)

    $ ./scripts/import_fixtures

The pull down an entire copy of the latest database and media files:
(this will include all data so running the above import and migrate commands will not be needed)
(Note: you'll need to have ssh public/private keys in place on the server for this to work)

    $ ./scripts/pull_data_from_prod

Create the media directories

    $ mkdir -p /data/lawbook/media /data/lawbook/static

Build the css/js files for client

  Development (recompiles when saving files)

    $ npm run-script watch

  Production (builds minified version)

    $ npm run-script prod

To bundle up all the static assets for delivery to the web server:

  Development (creates symlinks so changes will automatically take effect):

    $ django-admin collectstatic --link --noinput

  Production:

    $ django-admin collectstatic --noinput


Two other dependencies which are required for cache and search to work properly are redis and elasticsearch:

  Redis
    $ brew install redis
    $ redis-server

  Elasticsearch
    $ brew install elasticsearch
    $ elasticsearch

To run local instance via local django server:

    $ django-admin runserver_plus

Open your browser of choice and go to http://127.0.0.1:8000/

To log in to the Django admin go to: http://127.0.0.1:8000/admin/ and use the login credentials which were used when creating the superuser above.


Mirroring the Production environment

To run local instance behind nginx, modify the nginx-local.conf and nginx-prod.conf files to make sure the paths are correct based on your environment and move them into place:

  Local development:

    $ brew install nginx
    $ cp ./config/nginx/nginx-local.conf /etc/nginx/sites-available/lawbook.iskcon.org
    $ ln -s ../sites-available/lawbook.iskcon.org /etc/nginx/sites-enabled/lawbook.iskcon.org

  Production environment:

    $ cp ./config/nginx/nginx-prod.conf /etc/nginx/sites-available/lawbook.iskcon.org
    $ ln -s ../sites-available/lawbook.iskcon.org /etc/nginx/sites-enabled/lawbook.iskcon.org

Start the wsgi application:

    $ uwsgi --ini ./config/uwsgi.ini

Add en entry in your /etc/hosts file for the application:

    $ sudo echo "127.0.0.1 lawbook lawbook.dev lawbook.localhost" >> /etc/hosts

Restart nginx

    $ sudo nginx -s reload

The application should now load at http://lawbook.dev/

The Admin should load at http://lawbook.dev/admin

If you see any 50x errors, check the paths in the following files to make sure everything matches:

  /etc/environments/local/lawbook.ini
  /etc/nginx/sites-enabled/lawbook.iskcon.org

To copy static files (js, css, imgs, etc.) the following django commands are useful:

  create symlinks (for development, so updates are automatically refreshed):

    $ django-admin collectstatic --link --noinput

  copy the files instead of creating symlinks (for production environments):

    $ django-admin collectstatic --noinput

  remove all existing files (for fresh start):

    $ django-admin collectstatic --clear

   
  remove and re-link all files (first deletes, then links) 
  
    $ django-admin collectstatic --link --noinput --clear


Data APIs

There are three apis to retrieve data:

  Resolutions:   /api/v1/r/  #(includes the user object)
  Tags:          /api/v1/tags/
  Filters:       /api/v1/filters/

The default format is XML, but you can pass ?format=json to specify another format (most ajax calls will automatically request json so the ?format= parameter is usually only needed to return json to the browser for debugging purposes:

  http://lawbook.dev/api/v1/r/?format=json
