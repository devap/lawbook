"use strict"

sidebar = require('javascript/sidebar')

# Application bootstrapper.
App ->
  init: function init()

    $(document).foundation()
    require('javascript/lawbook_libs').init()
    console.log('App initialized.')
    $('body').prepend('<div class="msg">[JQ] App initialized.</div>')



module.exports = App;
