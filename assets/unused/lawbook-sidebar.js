//if (year1 != 1975 || year2 != 2016) {
if (year2 || date_range) {
  $('#clear-dates').show();
}

if (location.href.indexOf("/tag") > 0) {
  $('#clear-tags').show();
}

$("#dateSlider").dateRangeSlider({
  bounds: {
    min: new Date(dateSliderBoundsMin, 0, 1),
    max: new Date(dateSliderBoundsMax, 0, 1)
  },

  defaultValues:{
    //min: new Date(1975, 0, 1),
    //max: new Date(2016, 11, 31)
    min: new Date(dateSliderDefaultMin, 0, 1),
    max: new Date(dateSliderDefaultMax, 0, 1)
  },
  step:{ years: 1 },

  formatter:function(val){
    var year = val.getFullYear();
  return year;
  }
});

$("#dateSlider").bind("userValuesChanged", function(e, data){
  var y1 = data.values.min.getFullYear();
  var y2 = data.values.max.getFullYear();
  var str_tags = (tags ? tags : "/");
  var new_loc = "/" + y1 + "/" + y2 + str_tags;
  location.href = new_loc;
});
