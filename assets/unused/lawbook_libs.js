"use strict";

// Application bootstrapper.
var Lawbook = {
  init: function init() {

    var fullpath  = location.pathname;
    var dates     = false;
    var date_range  = false;
    var re_tags   = /(\/tag.*)/;
    var regex_tags  = re_tags.exec(fullpath);
    var tags    = regex_tags ? regex_tags[0] : false;

    if (year2 > year1) {
      date_range = true;
      dates = "/" + year1 + "/" + year2;
    } else if (year1) {
      dates = "/" + year1;
    }

    var first_year = 1975;
    var last_year = this_year;
    var dateSliderDefaultMin = year1 > 0 ? year1 : first_year;
    var dateSliderDefaultMax = year2 > 0 ? year2 : last_year;
    var dateSliderBoundsMin = first_year;
    var dateSliderBoundsMax = last_year;



    var clearFilter = function (elemId) {
      var new_loc = "";
      if (elemId === "clear-dates") {
        new_loc = tags ? tags : "/";
      }
      if (elemId === "clear-tags") {
        new_loc = dates ? dates : "/";
      }
      location.href = new_loc;
      //console.log("clearFilter - new_loc: " + new_loc);
      return false;
    }

    var chkFrm = function(form) {
      this_loc = location.pathname;
      if (form.id === "select-single-year") {
        if (form.yr.value === "") {
          alert("Selet a year to filter on.");
          return false;
        } else if (form.yr.value.length != 4) {
          alert("enter valid year (yyyy)");
          return false;
        } else {
          new_loc = "/" + form.yr.value + (tags ? tags : "");
        }
      }
      location.href = new_loc
      return false;
    }

    var tagsFilter = function(form) {
      var new_loc = dates ? dates : "";
      var str_tags = "/tags/";
      var elements = document.getElementById(form.id).elements;
      for (var i=0, element; element = elements[i++];) {
        if (element.type === "checkbox" && element.checked) {
          str_tags += element.name + "/";
        }
      }
      if (str_tags != "/tags/") {
        new_loc += str_tags;
      } else {
        alert ("Please select at least one tag to filter on.")
      }

    location.href = new_loc;
      //console.log("tagsFilter("+form.id+"): new_loc: " + new_loc);
      return false;
    }

    var clearTag = function (slug) {
      var this_loc = location.pathname;
      var new_loc = this_loc.replace(slug+"/", "");
      if (new_loc == "/tags/" || new_loc =="/tag/") {
        new_loc = dates ? dates : "/";
      }
      location.href = new_loc
      //console.log("clearTag: new_loc: " + new_loc);
      return false;
    }

    var loadLogin = function (modal) {

      var $modal = $(modal);

     $.ajax('/accounts/login')
       .done(function(resp){
         $modal.html(resp.html).foundation('open');
     });

    console.log('loadLogin(' + $modal + ')');
    return false;
  }


    $('#login_ajax').click(function( event ){

    event.preventDefault();
    console.log('#login_ajac.clicked(): ' + event);

    var $modal = $('#loginModal2');
    $.ajax('/accounts/login')
     .done(function(resp){
       $modal.html(resp.html).foundation('open');
    });
  });

    console.log("lawbook_libs initialization ...");
    console.log("year1: " + year1);
    console.log("year2: " + year2);
    console.log("dates: " + dates);
    console.log("date_range:" + date_range);
    console.log("tags: " + tags);

  }
}

module.exports = Lawbook;
