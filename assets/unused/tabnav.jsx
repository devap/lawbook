import React from 'react';






let Tab = {

  var Tabnav = React.createClass({
    propTypes: {
        SortBy: React.PropTypes.string.isRequired
      },
    getDefaultProps: () =>
      SortBy: 'date',

    render: () =>
      return
      (
        <div id="tabnav">
          <ul>
            <li>{this.props.SortBy}</li>
          </ul>
        </div>
      )
  });

  ReactDOM.render(
    <Tabnav />,
    document.getElementById('content')
  );

};

  console.log("tabnav initialized")

export default Tab;
