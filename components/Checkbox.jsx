
class Checkbox {

  toggleCheckbox (event) {
    console.clear();
    this.props.handleCheckboxChange(event.target.checked, this.props.tagid, this.props.value, this.props.label);
  }

  render() {
    return (
      <div className="checkbox" >
        <label>
          <input
            type="checkbox"
            value={this.props.value}
            checked={this.props.isChecked}
            onChange={this.toggleCheckbox}
          />

          {this.props.label}
        </label>
      </div>
    );
  }

};

export default Checkbox;
