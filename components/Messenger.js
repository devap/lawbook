
class Messenger {

  getMsg() {
    let strMsg = "";

    //if (this.props.user.is_authenticated && this.props.user.group == 'Admin') {
    if (this.props.user.is_authenticated && this.props.user.is_superuser) {
      strMsg = <div>rescount: {this.props.rescount}</div>;
    }
    return strMsg;
  }

  getMsgDebug () {
    let strDebug = "";
    let tags = this.props.filter_tags;
    let strTags = "";
    for (var t=0, tlen = tags.length; t<tlen; t++) {
      if (t < tlen-1) {
        strTags += tags[t] + " | ";
      } else {
        strTags += tags[t];
      }
    }

    //if (this.props.user.is_authenticated && this.props.user.group == 'Admin') {
    if (this.props.user.is_authenticated && this.props.user.is_superuser) {
       strDebug = <div className="msg-debug">
         <div>years: {this.props.filter_years[0]}&ndash;{this.props.filter_years[1]}</div>
         <div>tags: {strTags}</div>
         <div>searchTerm: {this.props.filter_searchTerm}</div>
       </div>;
     }
    return strDebug;
  }

  render () {
    return (
      <div className="msg">
        {this.getMsg()}
        {this.getMsgDebug()}
      </div>
    )
  }

};

console.log('Messenger initialized');

export default Messenger;
