
class NoResolutions extends React.Component {

  render() {

    let klass="no_resolutions";
    let strMsg = "Sorry, there are no resolutions matching that criteria.";

    if (!this.props.user.is_authenticated) {
      klass="login_required";
      strMsg = "Please login to proceed.";
    }

    return (
      <li className={klass} key='k'>
        {strMsg}
      </li>
    );

  }

}

export default NoResolutions;
