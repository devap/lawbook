import {createFilter} from 'react-search-input';
import SearchBar from 'SearchBar';
import TabNav from 'TabNav';
import Sidebar from 'Sidebar';
import Messenger from 'Messenger';
import ErrorBoundary from 'ErrorBoundary';

const KEYS_TO_FILTERS = ['title', 'tags', 'resolution'];


class ResolutionsApp extends React.Component {

  now() {
    let _time = new Date();
    let _sec = _time.getSeconds();
    let _msec = _time.getMilliseconds();
    let _strNow = 'time: ' + _sec + ':' + _msec;
    return _strNow;
  }

  conLog(msg) {
    console.log(msg);
  }

  constructor(props) {
    super(props);
    let now = new Date();
    const year1 = 1975;
    const year2 = now.getFullYear();

    this.state = {
      data_uri: '/api/v1/r/'
      ,tags_uri: '/api/v1/tags/'
      ,filters_uri: '/api/v1/filters/'
      ,resdata: []
      ,slider_default_years: [year1, year2]
      ,tags_common: []
      ,tags_all: []
      ,tags_checked: []
      ,sortBy: 'res_id'
      ,sortOrder: 'desc'
      ,filters: []
      ,filter_searchTerm: ''
      ,filter_years: [year1, year2]
      ,filter_tags: []
      ,messages: ''
      ,user: {}
    };
  }

  getGroups(strGroups) {
    this.conLog('getGroups(): strGroups: ' + strGroups);
    //cleanup funky django funky text-string-object
    //let re = /\[\<Group:\s(\w+)\>\]/;
    //let dj_group = re.exec(data['meta']['user']['group']);
    //extract the name of the group from the string
    let re = /.*u'(\w+)'.*/
    let groups = strGroups.split(',')
    /***
    let groups = []
    let pieces = strGroups.split(',')
    for (var i=0; i<pieces.length; i++) {
      groups.push(re.exec(pieces[i])[1]);
    }
    */
    this.conLog('returning groups: ' + groups)
    return groups;
  }

  componentDidMount() {
    const _url_data = this.state.data_uri;
    const _url_tags = this.state.tags_uri;
    const _url_filters = this.state.filters_uri;
    //get the resolutions
    $.ajax({
      url: _url_data,
      dataType: 'json',
      cache: true,
      success: function(data) {
        let _resdata = _.sortBy(data['objects'], 'res_id');
        let dj_user = {
          username: data['meta']['user']['username'],
          name: data['meta']['user']['name'],
          is_authenticated: data['meta']['user']['is_authenticated'],
          is_superuser: data['meta']['user']['is_superuser'],
          api_key: data['meta']['user']['api_key'],
          groups: this.getGroups(data['meta']['user']['groups'])
        };
        this.setState({
          resdata: _resdata,
          user: dj_user
        });
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(_url_data, status, err.toString());
      }.bind(this)
    });

    //get the tags
    $.ajax({
      url: _url_tags,
      dataType: 'json',
      cache: true,
      success: function(data) {
        let _common_tags = _.sortBy(data['meta']['most_used'], 'slug');
        let _all_tags = _.sortBy(data['objects'], 'slug');
        this.setState({
          tags_common: _common_tags,
          tags_all: _all_tags
        });
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(_url_tags, status, err.toString());
      }.bind(this)
    });

    //get the filters
    $.ajax({
      url: _url_filters,
      dataType: 'json',
      cache: true,
      success: function(data) {
        let _all_filters = _.sortBy(data['objects'], 'order');
        this.setState({
          filters: _all_filters
        });
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(_url_filters, status, err.toString());
      }.bind(this)
    });

  }

  componentWillUnmount() {
    this.serverRequest.abort();
  }

  updateSearch (e) {
    const _searchTerm = e.target.value;
    this.setState({
      filter_searchTerm: _searchTerm
    });
  }

  clearSearch () {
    this.setState({
      filter_searchTerm: ''
    });
  }

  sliderChange (value) {
    this.setState({
      filter_years: value,
    });
  }

  clearYears () {
    let now = new Date();
    let year1 = 1975;
    let year2 = now.getFullYear();
    this.setState({
      filter_years: [ year1, year2 ],
    });
  }

  filterTag (checked, id, slug) {

    let _tag_slugs =  this.state.filter_tags.slice();
    let _tag_ids =  this.state.tags_checked.slice();
    if (checked) {
      _tag_ids.push(id);
      _tag_slugs.push(slug);
    } else {
      _.pull(_tag_ids, id);
      _.pull(_tag_slugs, slug);
    }

    this.setState({
      tags_checked: _tag_ids,
      filter_tags: _tag_slugs,
    });
  }

  clearTags () {
    this.setState({
      tags_checked: [],
      filter_tags: []
    });
  }

  render() {

    let _filtered_data =  this.state.resdata;

    // years:
    let _years = this.state.filter_years;
    if (_years.length > 0) {
      _filtered_data = _.filter(_filtered_data, function(r) {
        let has_match = _.gte(r.year, _years[0]) && _.lte(r.year, _years[1]);
        return has_match;
      });
    }

    // search:
    let _searchTerm = this.state.filter_searchTerm;
    this.conLog('render():_searchTerm: ' + _searchTerm);
    if (_searchTerm.length > 0) {
      _filtered_data = _.filter(_filtered_data, createFilter(_searchTerm, KEYS_TO_FILTERS));
    }

    // tags
    let _tag_slugs =  this.state.filter_tags;
    let _tag_ids =  this.state.tags_checked;
    let found_tag = false;
    if (_tag_slugs.length > 0) {
      _filtered_data = _.filter(_filtered_data, function(r) {
        let has_match = false;
        _.forEach(_tag_slugs, function(s) {
          let found_tag = false;
          found_tag = _.some(r.tags, { 'slug': s } );
          console.log('found_tag: ' + found_tag);
          if (found_tag) {
            has_match = true;
          }
        });
        return has_match;
      });
    }

    this.conLog('render(after):_filtered_data: ' + _filtered_data.length);


    const sidebarProps = {
      sliderChange: this.sliderChange.bind(this)
      ,slider_default_years: this.state.slider_default_years
      ,clearYears: this.clearYears.bind(this)
      ,filters: this.state.filters
      ,filterTag: this.filterTag.bind(this)
      ,clearTags: this.clearTags.bind(this)
      ,tags_all: this.state.tags_all
      ,tags_common: this.state.tags_common
      ,tags_checked: this.state.tags_checked
      ,user: this.state.user
    };

    const searchProps = {
      updateSearch: this.updateSearch.bind(this)
      ,clearSearch: this.clearSearch.bind(this)
      ,user: this.state.user
    };

    const resProps = {
      resolutions: _filtered_data
      ,user: this.state.user
    };

    const msgProps = {
      rescount: _filtered_data.length
      ,filter_tags: this.state.filter_tags
      ,filter_years: this.state.filter_years
      ,filter_searchTerm: this.state.filter_searchTerm
      ,user: this.state.user
    };

    return (
      <div className="row">
        <Sidebar {...sidebarProps} />
        <div id="main" className="col-9">
          <SearchBar {...searchProps} />
          <ErrorBoundary {...msgProps}>
          <Messenger {...msgProps} />
          </ErrorBoundary>
          <TabNav {...resProps} />
        </div>
      </div>
    );
  }
};


console.log("ResolutionsApp initialized");

export default ResolutionsApp;
