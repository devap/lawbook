
class ResolutionsGroup extends React.Component {
  render() {
    return (
      <div className="group-header">{this.props.group}</div>
    );
  }
}

//console.log("ResolutionsGroup initialized");

export default ResolutionsGroup;
