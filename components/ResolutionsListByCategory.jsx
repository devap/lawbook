import TabLinks from 'TabLinks';
import ResolutionsRow from 'ResolutionsRow';
import ResolutionsGroup from 'ResolutionsGroup';
import NoResolutions from 'NoResolutions';

class ResolutionsListByCategory extends React.Component {

  render() {
    let rows = [];
    let lastCategory = null;
    let resdata = this.props.resolutions;
    let rs = _.orderBy(resdata, 'category', 'asc');

    if (rs.length > 0) {

      for (var i = 0, len = rs.length; i < len; i++) {
        if (rs[i].category && rs[i].category != lastCategory) {
          let keyID = i + '.' + rs[i].category;
          rows.push(<ResolutionsGroup group={rs[i].category} key={keyID} />);
          lastCategory = rs[i].category;
        }

        rows.push(<ResolutionsRow user={this.props.user} resolution={rs[i]} key={rs[i].res_id} />);
      };

    } else {
        rows.push(<NoResolutions user={this.props.user} key="9" />);
    }

    return (
      <div>
        <TabLinks user={this.props.user} />
        <ul className="resolutions-items attribs-show resolutions-list">
          {rows}
        </ul>
      </div>
    );
  }

}

console.log("ResolutionsListByCategory initialized");

export default ResolutionsListByCategory;
