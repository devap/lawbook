import TabLinks from 'TabLinks';
import ResolutionsRow from 'ResolutionsRow';
import ResolutionsGroup from 'ResolutionsGroup';
import NoResolutions from 'NoResolutions';

class ResolutionsListByDate extends React.Component {

  render() {
    let rows = [];
    let lastYear = null;
    const resdata = this.props.resolutions;
    let rs = _.orderBy(resdata, ['year', 'sort_order'], ['desc', 'asc']);

    if (rs.length > 0) {

      for (var i = 0; i < rs.length; i++) {
        if (rs[i].year != lastYear) {
          rows.push(<ResolutionsGroup group={rs[i].year} key={rs[i].year} />);
          lastYear = rs[i].year;
        }
        rows.push(<ResolutionsRow user={this.props.user} resolution={rs[i]} key={rs[i].res_id} />);
      };

    } else {
        rows.push(<NoResolutions user={this.props.user} key="9" />);
    }

    return (
      <div>
        <TabLinks user={this.props.user} />
        <ul className="resolutions-items attribs-show resolutions-list">
          {rows}
        </ul>
      </div>
    );
    }

}

//console.log("ResolutionsListByDate initialized");

export default ResolutionsListByDate;
