import TabLinks from 'TabLinks';
import ResolutionsRow from 'ResolutionsRow';
import ResolutionsGroup from 'ResolutionsGroup';
import NoResolutions from 'NoResolutions';

class ResolutionsListBySection extends React.Component {

  sortit(a,b) {
    return(a-b);
  }

  render() {
    let rows = [];
    let lastSection = null;
    let resdata = this.props.resolutions;
    //let rs = _.orderBy(rs_bydate, [Number(rs_bydate.section_id), 'year'], ['asc', 'desc'] );
    //let rs = rs_bydate.sort(this.sortit);
    let rs = _.orderBy(
      resdata,
      function(r) { return parseFloat(r.section_id); },
      'asc'
    );


    if (rs.length > 0) {

      for (var i = 0, len = rs.length; i < len; i++) {
        if (rs[i].section && rs[i].section !== lastSection) {
          let keyID = i + '.' + rs[i].section;
          rows.push(<ResolutionsGroup group={rs[i].section} key={keyID} />);
          lastSection = rs[i].section;
        }
        rows.push(<ResolutionsRow user={this.props.user} resolution={rs[i]} key={rs[i].res_id} />);
        };

      } else {
          rows.push(<NoResolutions user={this.props.user} key="9" />);
      }

    return (
      <div>
        <TabLinks user={this.props.user} />
        <ul className="resolutions-items attribs-show resolutions-list">
          {rows}
        </ul>
      </div>
    );
    }

}

//console.log("ResolutionsListBySection initialized");

export default ResolutionsListBySection;
