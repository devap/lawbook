import TabLinks from 'TabLinks';
import ResolutionsRow from 'ResolutionsRow';
import NoResolutions from 'NoResolutions';

class ResolutionsListByTitle extends React.Component {

  render() {
    let rows = [];
    let resdata = this.props.resolutions;
    let rs = _.orderBy(resdata, 'title', 'asc');

    if (rs.length > 0) {

      for (var i = 0, len = rs.length; i < len; i++) {
        rows.push(<ResolutionsRow user={this.props.user} resolution={rs[i]} key={rs[i].res_id} />);
      };

    } else {
        rows.push(<NoResolutions user={this.props.user} key="9" />);
    }

    return (
      <div>
        <TabLinks user={this.props.user} />
        <ul className="resolutions-items attribs-show resolutions-list">
          {rows}
        </ul>
      </div>
    );
  }

}

//console.log("ResolutionsListByTitle initialized");

export default ResolutionsListByTitle;
