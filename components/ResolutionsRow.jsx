import TagsList from 'TagsList';

class ResolutionsRow extends React.Component {

  rawMarkupResolution() {
    let res_text = this.props.resolution.resolution;
    return { __html: res_text };
  }

  rawMarkupPresentation() {
    let pres_text = this.props.resolution.presentation;
    return { __html: pres_text };
  }

  rawMarkupCommentsGBC() {
    let comments_gbc = this.props.resolution.comments_gbc;
    return { __html: comments_gbc };
  }

  rawMarkupCommentsDeputy() {
    let comments_deputy = this.props.resolution.comments_deputy;
    return { __html: comments_deputy };
  }

  get_taghead() {
    let type = typeof this.props.user.groups;
    let isDev = _.includes(this.props.user.groups, 'Developer');
    if (isDev) {
      return (
        <div className="item-label">Tags: </div>
      );
    }
  }

  get_taglist() {
    let isDev = _.includes(this.props.user.groups, 'Developer');
    if (isDev) {
      return (
        <TagsList user={this.props.user} tags={this.props.resolution.tags} />
      );
    }
  }

  openAttributes(event) {
    event.preventDefault();
    console.log('openAttributes():event.target: '+event.target);
    console.log('openAttributes():event.target.href: '+event.target.href);
    const divID = event.target.id;
    console.log('divID: ' +divID);
    $(divID).slideToggle(300);
  }

  render() {
    let status_str = '';
    let status_cls = 'pub_status';
    let class_status = 'resolution-item';
    let containerHREF = '#' + this.props.resolution.res_id;
    let containerID = this.props.resolution.res_id;

    if (!this.props.resolution.published) {
      status_str = 'Unpublished';
      status_cls += ' unpublished';
      class_status += ' unpublished';
    }

    let attribStyle = { display: 'none' };
    let isDev = _.includes(this.props.user.groups, 'Developer')
    if (isDev) {
      attribStyle = { display: 'show' };
    }

    return (
      //these are all children of <ul className="resolutions-items">
      <li className={class_status}>
        <div className={status_cls}>{status_str}</div>
        <a id={containerHREF} href="#" className="title-link" onClick={this.openAttributes}>{this.props.resolution.title}</a>
        <div className="attributes">
          <div><span className="item-label">Year: </span> {this.props.resolution.year}</div>
          {this.get_taghead()}
          {this.get_taglist()}
          <div className="item-label">Resolution: </div>
          <div dangerouslySetInnerHTML={this.rawMarkupResolution()} />


          <div id={containerID} style={attribStyle}>
            <div><span className="item-label">Section: </span> {this.props.resolution.section}</div>
            <div><span className="item-label">Category:</span> {this.props.resolution.category}</div>
            <div><span className="item-label">Presentation:</span>
              <div dangerouslySetInnerHTML={this.rawMarkupPresentation()} />
            </div>
            <div><span className="item-label">GBC Discussions:</span>
              <div dangerouslySetInnerHTML={this.rawMarkupCommentsGBC()} />
            </div>
            <div><span className="item-label">Deputy Discussions:</span>
              <div dangerouslySetInnerHTML={this.rawMarkupCommentsDeputy()} />
            </div>
            <div><span className="item-label">ID: </span> {this.props.resolution.res_id}</div>
          </div>

        </div>
      </li>
    );
  }
}

console.log("ResolutionsRow initialized");

export default ResolutionsRow;
