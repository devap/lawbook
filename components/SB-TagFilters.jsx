import Checkbox from 'Checkbox';
import Collapsible from 'react-collapsible';
import FontAwesome from 'react-fontawesome';

class SidebarTagFilters {

  now () {
    let _time = new Date();
    let _sec = _time.getSeconds();
    let _msec = _time.getMilliseconds();
    let _strNow = 'time: ' + _sec + ':' + _msec;
    return _strNow;
  }

  // taglist is an array of id#'s coming from { state: tags_checked }
  // passed in via props
  checkState (taglist, id) {
    if (taglist.length <= 0) {
      return false;
    }
    let has_match = false;
    for (let i=0; i<taglist.length; i++) {
      if (id == taglist[i]) {
        has_match = true;
      }
    }
    return has_match;
  }

  uncheckAll () {
    this.props.clearTags();
  }

  toggleCheckbox (checked, id, slug, label) {

    if (this.selectedCheckboxes.has(label)) {
      this.selectedCheckboxes.delete(label);
    } else {
      this.selectedCheckboxes.add(label);
    }
    this.props.filterTag(checked, id, slug);
  }

  componentWillMount () {
    this.selectedCheckboxes = new Set();
  }

  createCommonCheckboxes (tags, tags_checked) {
    let _this = this;
    let ctags_rows = [];
    tags.map(function(obj, idx) {
      let cbref = "chkboxC-" + idx;
      let tagid = obj.id + "C";
      //return <Checkbox
      ctags_rows.push(<Checkbox
        ref={cbref}
        tagid={tagid}
        label={obj.name}
        value={obj.slug}
        isChecked={_this.checkState(tags_checked, tagid)}
        handleCheckboxChange={_this.toggleCheckbox}
        key={cbref}
                      />);
    });

    return ctags_rows;
  }

  createAllTagsTab () {
    let strAllTagsTab = "";
    if (this.props.user.is_authenticated && this.props.user.is_superuser) {
      strAllTagsTab = <li className="tabs-title"><a href="#panel-tags-all">All Tags</a></li>;
    }
    return strAllTagsTab;
  }

  createAllCheckboxes (tags, tags_checked) {
    let _this = this;
    let alltags = [];
    let alltags_rows = [];
    for (var i=0; i<tags.length; i++) {
      let newObj = {
        id: tags[i].id,
        name: tags[i].name,
        url: tags[i].resource_uri,
        slug: tags[i].slug
      };
      alltags.push(newObj);
    }

    for (var i=0; i<alltags.length; i++) {
      let cbref = "chkboxA-" + i;
      let tagid = alltags[i].id;
      alltags_rows.push(<Checkbox
        ref={cbref}
        tagid={tagid}
        label={alltags[i].name}
        value={alltags[i].slug}
        isChecked={_this.checkState(tags_checked, tagid)}
        handleCheckboxChange={_this.toggleCheckbox}
        key={cbref}
                        />
      );
    }

    if (this.props.user.is_authenticated && this.props.user.is_superuser) {
      console.log('SUPERUSER');
      return <div className="tabs-panel" id="panel-tags-all">
        <form id="frm_all-tags">
          {alltags_rows}
        </form>
      </div>;
    } else {
      console.log('NOT AUTHORIZED');
    }
  }

  createFilterList (filters, tags_checked) {
    let _this = this;
    let filterDiv = "";
    let filtersNtags_rows = [];
    let status = "";
    let transitionTime = 200;
    for (var f=0; f<filters.length; f++) {
      let filtername = filters[f].name;
      let filterslug = filters[f].slug;
      //create list for holding tags
      let tags = _.sortBy(filters[f].tags, 'slug');
      let tags_rows = [];

      console.log('filtername: ' + filtername + ' (' + tags.length + ')' );

      // create checkboxes for each tag within filter group
      for (var i=0; i<tags.length; i++) {
        let cbref = filtername + "-chkboxA-" + i;

        tags_rows.push(<Checkbox
          ref={cbref}
          tagid={tags[i].id}
          label={tags[i].name}
          value={tags[i].slug}
          isChecked={_this.checkState(tags_checked, tags[i].id)}
          handleCheckboxChange={_this.toggleCheckbox}
          key={cbref}
        />);
      }

      // output filter header and name
      filterDiv = <Collapsible
                    trigger={filtername}
                    transitionTime={transitionTime}
                    key={f}
                  >
                    {tags_rows}
                  </Collapsible>

      //output filter name as header row
      filtersNtags_rows.push(filterDiv);

    }

    return filtersNtags_rows;
  }

  render () {
    return (
      <div id="tagdiv">
        <ul className="tabs" data-tabs id="taglist">
          <li className="tabs-title is-active"><a href="#panel-tags-common" aria-selected="true">Top-Ten Tags</a></li>
          <li className="tabs-title"><a href="#panel-tags-filters">Filtered Tags</a></li>
          {this.createAllTagsTab()}
        </ul>
        <div className="tabs-content" data-tabs-content="taglist">
          <div className="tabs-panel is-active" id="panel-tags-common">
            <form id="frm_top-ten-tags">
              {this.createCommonCheckboxes(this.props.tags_common, this.props.tags_checked)}
            </form>
          </div>

          <div className="tabs-panel" id="panel-tags-filters">
            <form id="frm_all-filters">
              {this.createFilterList(this.props.filters, this.props.tags_checked)}
            </form>
          </div>

              {this.createAllCheckboxes(this.props.tags_all, this.props.tags_checked)}

        </div>

        <button className="button clear-tags" onClick={this.uncheckAll}>Clear Tags</button>

      </div>
    );
  }

};

console.log('SB-TagFilters initialized');

export default SidebarTagFilters;
