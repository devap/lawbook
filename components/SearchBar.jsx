
class SearchBar extends React.Component {
  render() {
    let Search = <div className="hide">No SearchBar</div>;
    if (this.props.user.is_authenticated) {
      Search =
        <form className="search-wrapper">
          <input type="search" className="search-input" onChange={this.props.updateSearch} required placeholder="Search..."   />
          <button type="reset" className="close-icon" onClick={this.props.clearSearch}></button>
        </form>;
    }

    return Search;
  }

}


export default SearchBar;
