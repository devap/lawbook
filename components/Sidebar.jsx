import SidebarTagFilters from 'SB-TagFilters';
import ErrorBoundary from 'ErrorBoundary';

class Sidebar extends React.Component {

  render() {

    const sliderProps = {
      min: this.props.slider_default_years[0],
      max: this.props.slider_default_years[1],
      allowCross: false,
      range: true,
      defaultValue: this.props.slider_default_years,

      //onChange: this.props.sliderChange.bind(this)
      onAfterChange: this.props.sliderChange.bind(this),
      clearYears: this.props.clearYears.bind(this)
    };

    const tagfilterProps = {
        filterTag: this.props.filterTag.bind(this),
        clearTags: this.props.clearTags.bind(this),
        filters: this.props.filters,
        tags_all: this.props.tags_all,
        tags_common: this.props.tags_common,
        tags_checked: this.props.tags_checked,
        user: this.props.user
    };

    let loginKlass = "col-3";
    if (this.props.user.is_authenticated) {
      loginKlass += " show";
    } else {
      loginKlass += " hide";
    }


    return (
      <div id="sidebarL" className={loginKlass}>
        <div className="slider-label">Select Date Range:</div>
        <ErrorBoundary {...tagfilterProps}>
          <SidebarTagFilters {...tagfilterProps} />
        </ErrorBoundary>
      </div>
    );

    }
}

console.log('components/Sidebar initialized');

export default Sidebar;
