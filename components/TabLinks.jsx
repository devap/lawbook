import { NavLink } from 'react-router-dom';

class TabLinks extends React.Component {

  render() {
    let Links = <div className="hide">No TabNav</div>;
    if (this.props.user.is_authenticated) {
      Links =
      <ul id="tabnav">
        <li className="tabs">
          <NavLink to="/" exact className="tablink"activeClassName="is-active">
             ByDate
          </NavLink>
        </li>
        <li className="tabs">
          <NavLink to="/section" className="tablink" activeClassName="is-active">
            By Section
          </NavLink>
        </li>
        <li className="tabs">
          <NavLink to="/category" className="tablink" activeClassName="is-active">
            By Category
          </NavLink>
        </li>
        <li className="tabs">
          <NavLink to="/title" className="tablink" activeClassName="is-active">
            By Title
          </NavLink>
        </li>
      </ul>;
    }

    return Links;
  }
}

export default TabLinks;
