import {BrowserRouter as Router, Route, Link } from 'react-router-dom'

import ResolutionsListByDate from 'ResolutionsListByDate';
import ResolutionsListBySection from 'ResolutionsListBySection';
import ResolutionsListByCategory from 'ResolutionsListByCategory';
import ResolutionsListByTitle from 'ResolutionsListByTitle';

class TabNav extends React.Component {

  render() {

    const resProps = {
      resolutions: this.props.resolutions,
      user: this.props.user
    };

    const App = (props) => (
      <div>
        <Route exact path='/' component={() => (<ResolutionsListByDate {...resProps} />)} />
        <Route path='/loginrequired' component={() => (<ResolutionsListByDate {...resProps} />)} />
        <Route path='/section' component={() => (<ResolutionsListBySection {...resProps} />)} />
        <Route path='/category' component={() => (<ResolutionsListByCategory {...resProps} />)} />
        <Route path='/title' component={() => (<ResolutionsListByTitle {...resProps} />)} />
      </div>
    );

    return(
          <Router>
            <App {...resProps} />
          </Router>
    );
  }

}

//console.log("TabNav initialized");

export default TabNav;
