
class TagItem extends React.Component {
  render() {
    return(
      <a className="tag-item" href={this.props.tag.resource_uri}>{this.props.tag.name}</a>
    );
  }
}

class TagsList extends React.Component {
  render() {
    let tags = this.props.tags;
    let tag_list = '[no tags]';
    if (tags) {
      tag_list = [];
      for (var i = 0, len = tags.length; i < len; i++) {
        tag_list.push(<TagItem tag={tags[i]} key={tags[i].id} />);
        if (i < len - 1) {
          tag_list.push(" | ");
        }
      }
    }

    return (
      <div className="tag-list">
        {tag_list}
      </div>
    );
    }
  }

console.log("TagsList initialized");

export default TagsList;
