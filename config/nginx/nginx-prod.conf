server {
  server_name
    lawbook.iskcon.*
    .lawbook.iskcon.org
  ;

  root /var/www/lawbook.iskcon.org;

  if ($http_host !~ "^lawbook.iskcon.org$") {
    rewrite ^/(.*)$ http://lawbook.iskcon.org/$1 permanent;
  }

  if ($http_x_forwarded_proto != 'https') {
    return https://$host$request_uri;
  }

#  if ($request_uri ~ "^/admin/.*$") {
#    rewrite ^/(.*)$ http://admin.lawbook.iskcon.org/$1 permanent;
#  }

#  if ($request_uri ~ "^/feed/.*$") {
#    rewrite ^/(.*)$ https://feeds.lawbook.iskcon.org/$1 permanent;
#  }

#  if ($request_uri ~ "^/preview/.*$") {
#    rewrite ^/(.*)$ https://preview.lawbook.iskcon.org/$1 permanent;
#  }

  location ~ (/\.ht|\.git) { deny all; }
  location ~ .*\.cache$ { deny all; }
  location ~ \..*/.*\.php$ { return 403; }

  #Prevent access to any files starting with a $ (usually temp files)
  location ~ ~$ { access_log off; log_not_found off; deny all; }

  location /files/ {
    root /data/lawbook/media;
    expires max;
  }

  location /media/ {
    root /data/lawbook;
    expires max;
  }

  location /static/ {
    root /data/lawbook;
    expires max;
  }

  location /imgvers/ {
    root /data/lawbook/media;
    expires max;
  }

  location /img/ {
    root /data/lawbook/static;
    expires max;
  }

  location /images/ {
    root /data/lawbook/media;
    expires max;
  }

  location /robots.txt {
    root /data/lawbook/static;
  }

  location ^~ /favicon.ico {
    rewrite ^/(.*)/$ /$1 permanent;
    alias /data/lawbook/static/ico/favicon.ico;
    log_not_found off;
    access_log off;
  }

  location ~ /apple-touch-icon.* {
    root /data/lawbook/static/img/touch;
    access_log off;
    log_not_found off;
  }

  # Finally, send all non-media requests to the Django server.
  location / {
    uwsgi_buffering off;
    include     uwsgi_params;
    uwsgi_pass_request_headers on;
    uwsgi_no_cache $cookie_nocache  $arg_nocache$arg_comment;
    uwsgi_no_cache $http_pragma     $http_authorization;
    uwsgi_cache_bypass $cookie_nocache $arg_nocache $arg_comment;
    uwsgi_cache_bypass $http_pragma $http_authorization;

    uwsgi_pass  unix:/var/run/uwsgi/app/lawbook.iskcon.org.sock;
  }

  error_page 404             /404.html;
  error_page 500 502 503 504 /500.html;

  error_log /var/log/nginx/lawbook.iskcon.org-error.log;
  access_log /var/log/nginx/lawbook.iskcon.org-access.log;

}
