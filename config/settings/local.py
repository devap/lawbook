# -*- coding: utf-8 -*-
from .base import *

from configparser import RawConfigParser
config = RawConfigParser()

#config_file = os.path.join(CONF_DIR, '.env-local.ini')
config_file = '/etc/environments/local/lawbook.iskcon.org.ini'

config.read(config_file)

"""
Local settings
- Run in Debug mode
- Use console backend for emails
- Add Django Debug Toolbar
- Add django-extensions as app
"""

DEBUG = True

DATABASES['default']['NAME'] = config.get('database', 'DATABASE_NAME')
DATABASES['default']['USER'] = config.get('database', 'DATABASE_USER')
DATABASES['default']['PASSWORD'] = config.get('database', 'DATABASE_PASSWORD')

SECRET_KEY = config.get('django', 'SECRET_KEY')

TEMPLATES[0]['OPTIONS']['debug'] = DEBUG
TEMPLATES[0]['OPTIONS']['loaders'] = [
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader'
]

ALLOWED_HOSTS = [
  "lawbook.localhost"
]

########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES['default']['LOCATION'] = config.get('cache', 'REDIS_URL')
CACHES['default']['KEY_PREFIX'] = config.get('cache', 'KEY_PREFIX')
########## END CACHE CONFIGURATION

########## SEARCH CONFIGURATION
HAYSTACK_CONNECTIONS['default']['INDEX_NAME'] = config.get('search', 'INDEX_NAME')
HAYSTACK_CONNECTIONS['default']['URL'] = config.get('search', 'URL')
########## END SEARCH CONFIGURATION

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Note: This key only used for development and testing.
SECRET_KEY = config.get('django', 'SECRET_KEY')
#SESSION_COOKIE_DOMAIN = config.get('secure', 'SESSION_COOKIE_DOMAIN')

### AWS
#AWSHOST = config.get('aws', 'AWSHOST')
AWS_ACCESS_KEY_ID = config.get('aws', 'AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = config.get('aws', 'AWS_SECRET_ACCESS_KEY')

# django-debug-toolbar
# ------------------------------------------------------------------------------
MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)

INSTALLED_APPS += (
  'django_extensions',
  'debug_toolbar',
)

INTERNAL_IPS = config.get('django', 'INTERNAL_IPS')

DEBUG_TOOLBAR_CONFIG = {
  'DISABLE_PANELS': [
    'debug_toolbar.panels.redirects.RedirectsPanel',
  ],
  'SHOW_TEMPLATE_CONTEXT': True,
}

DEBUG_TOOLBAR_PANELS = [
  'debug_toolbar.panels.versions.VersionsPanel',
  'debug_toolbar.panels.timer.TimerPanel',
  'debug_toolbar.panels.settings.SettingsPanel',
  'debug_toolbar.panels.headers.HeadersPanel',
  'debug_toolbar.panels.request.RequestPanel',
  'debug_toolbar.panels.sql.SQLPanel',
  'debug_toolbar.panels.cache.CachePanel',
  'debug_toolbar.panels.staticfiles.StaticFilesPanel',
  'debug_toolbar.panels.templates.TemplatesPanel',
  'debug_toolbar.panels.signals.SignalsPanel',
  'debug_toolbar.panels.logging.LoggingPanel',
  'debug_toolbar.panels.redirects.RedirectsPanel',
  'cachalot.panels.CachalotPanel',
]

# TESTING
# ------------------------------------------------------------------------------
#TEST_RUNNER = 'django.test.runner.DiscoverRunner'
