# -*- coding: utf-8 -*-
from requests_aws4auth import AWS4Auth
from boto.s3.connection import OrdinaryCallingFormat
from django.utils import six

from .base import *
config_file = '/etc/environments/production/lawbook.iskcon.org.ini'

from configparser import RawConfigParser
config = RawConfigParser()
config.read(config_file)

"""
Production Configurations
- Use Amazon's S3 for storing static files and uploaded media
- Use mailgun to send emails
- Use Redis for cache
"""

ALLOWED_HOSTS = [
  "lawbook.iskcon.org",
  "admin.lawbook.iskcon.org",
  "lawbook-admin.iskcon.org"
]

########## AWS CONFIGURATION
AWS_ES_HOST = config.get('aws', 'AWS_ES_HOST')
AWS_ACCESS_KEY_ID = config.get('aws', 'AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = config.get('aws', 'AWS_SECRET_ACCESS_KEY')

AWSAUTH = AWS4Auth(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, 'us-east-1', 'es')
AWS_AUTO_CREATE_BUCKET = True
AWS_QUERYSTRING_AUTH = False
AWS_S3_CALLING_FORMAT = OrdinaryCallingFormat()

# TODO See: https://github.com/jschneier/django-storages/issues/47
# Revert the following and use str after the above-mentioned bug is fixed in
# either django-storage-redux or boto
# AWS cache settings, don't change unless you know what you're doing:
AWS_EXPIRY = 60 * 60 * 24 * 7
AWS_HEADERS = {
  'Cache-Control': six.b('max-age=%d, s-maxage=%d, must-revalidate' % (
    AWS_EXPIRY, AWS_EXPIRY))
}
########## END AWS CONFIGURATION

SECRET_KEY = config.get('django', 'SECRET_KEY')
#SESSION_COOKIE_DOMAIN = config.get('django', 'SESSION_COOKIE_DOMAIN')

### Database
DATABASES['default']['NAME'] = config.get('database', 'DATABASE_NAME')
DATABASES['default']['USER'] = config.get('database', 'DATABASE_USER')
DATABASES['default']['PASSWORD'] = config.get('database', 'DATABASE_PASSWORD')
DATABASES['default']['HOST'] = config.get('database', 'DATABASE_HOST')


INSTALLED_APPS += (
    # See: http://django-storages.readthedocs.io/en/latest/index.html
    'storages',

    # Anymail with Sendgrid
    'anymail',
)
# Static Assets
# ------------------------
#STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

########## EMAIL CONFIGURATION
EMAIL_PORT = config.get('email', 'EMAIL_PORT')
EMAIL_HOST = config.get('email', 'EMAIL_HOST')
EMAIL_HOST_USER = config.get('email', 'EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = config.get('email', 'EMAIL_HOST_PASSWORD')
SENDGRID_API_KEY = config.get('email', 'SENDGRID_API_KEY')
ANYMAIL = {
    'SENDGRID_API_KEY': SENDGRID_API_KEY
}
########## END EMAIL CONFIGURATION

########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES['default']['LOCATION'] = config.get('cache', 'REDIS_URL')
CACHES['default']['KEY_PREFIX'] = config.get('cache', 'KEY_PREFIX')
########## END CACHE CONFIGURATION

########## SEARCH CONFIGURATION
HAYSTACK_CONNECTIONS['default']['INDEX_NAME'] = config.get('search', 'INDEX_NAME')
HAYSTACK_CONNECTIONS['default']['URL'] = AWS_ES_HOST
HAYSTACK_CONNECTIONS['default']['KWARGS'] = {
  'port': 443,
  'http_auth': AWSAUTH,
  'use_ssl': True,
  'verify_certs': True,
  'connection_class': elasticsearch.RequestsHttpConnection
}
########## END SEARCH CONFIGURATION

# Use Whitenoise to serve static files
# See: https://whitenoise.readthedocs.io/
#WHITENOISE_MIDDLEWARE = ('whitenoise.middleware.WhiteNoiseMiddleware', )
#MIDDLEWARE_CLASSES = WHITENOISE_MIDDLEWARE + MIDDLEWARE_CLASSES
