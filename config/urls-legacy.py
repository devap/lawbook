from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.views.static import serve

import views

urlpatterns = [

    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, include(admin.site.urls)),

    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^taggit_autosuggest/', include('taggit_autosuggest.urls')),

    url(
        regex=r'^$',
        view=views.ResolutionsListView.as_view(),
        name='list'
    ),

"""
    url(
        regex=r'^login/$',
        view=views.Login.as_view(),
        name='login'
    ),

    url(
        regex=r'^logout/$',
        view=views.Logout.as_view(),
        name='logout'
    ),
"""
    url(
        regex=r'^(?P<year1>\d{4})(?P<year2>\/\d{4})?/tags/(?P<tags>\w+.*)/$',
        view=views.ResolutionsListView.as_view(),
        name='years-tags'
    ),

    url(
        regex=r'^(?P<year1>\d{4})(?P<year2>\/\d{4})?/tag/(?P<tag>[-|\w|\d]+)/$',
        view=views.ResolutionsListView.as_view(),
        name='years-tagged'
    ),

    url(
        #regex=r'^(?P<year1>\d{4})/.*$',
        regex=r'^(?P<year1>\d{4})(?P<year2>\/\d{4})?/.*$',
        view=views.ResolutionsListView.as_view(),
        name='years'
    ),

    url(
        regex=r'^tags/(?P<tags>\w+.*)/$',
        view=views.ResolutionsListView.as_view(),
        name='tags'
    ),

    url(
        regex=r'^tag/(?P<tag>[-\s\w\d]+)/$',
        view=views.ResolutionsListView.as_view(),
        name='tagged'
    ),

    url(
        regex=r'^resolution/(?P<pk>\d+)/$',
        view=views.ResolutionsDetailView.as_view(),
        name='detail'
    ),

    # haystack
    #url(r'^search/$', include('haystack.urls')),
    url(
        regex=r'^search/$',
        view=views.CustomSearchView.as_view(),
        name='haystack_search'
    ),
]

if settings.DEBUG:

  urlpatterns += [
    url(r'^media/(.+)', serve, {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(.+)', serve, {'document_root': settings.STATIC_ROOT}),
    url(r'^files/(.+)', serve, {'document_root': settings.FILES_ROOT}),
  ]
