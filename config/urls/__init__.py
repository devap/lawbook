# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.views import defaults as default_views

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = []

from ._admin import admin_patterns
urlpatterns += admin_patterns

from ._tags import tag_patterns
urlpatterns += tag_patterns

from ._rest import rest_patterns
urlpatterns += rest_patterns

from lawbook.urls import lawbook_patterns
urlpatterns += lawbook_patterns

if settings.DEBUG:
  import debug_toolbar
  from django.conf.urls.static import static
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
  urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
  urlpatterns += [
    url(r'^__debug__/', include(debug_toolbar.urls)),

    url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
    url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
    url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
    url(r'^404\.html/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
    url(r'^500/$', default_views.server_error),

  ]


