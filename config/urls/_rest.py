from django.contrib import admin
from django.conf.urls import include, url
from tastypie.api import Api
from services.api import FilterResource, ResolutionResource, CategoryResource, SectionResource, TagResource

admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(FilterResource())
v1_api.register(ResolutionResource())
v1_api.register(CategoryResource())
v1_api.register(SectionResource())
v1_api.register(TagResource())

rest_patterns = [

  url(r'^api/', include(v1_api.urls)),

]


