from django.conf.urls import include, url
from lawbook import views

tag_patterns = [

  url(r'^taggit_autosuggest/', include('taggit_autosuggest.urls')),

  url(
      regex=r'^tags/(?P<tags>\w+.*)/$',
      view=views.ResolutionsListView.as_view(),
      name='tags'
  ),

  url(
      regex=r'^tag/(?P<tag>[-\s\w\d]+)/$',
      view=views.ResolutionsListView.as_view(),
      name='tagged'
  ),

]
