import 'Resolutions';

/*
lawbook libs
*/

window.loadLogin = function(modal) {
  //event.preventDefault();
  let $modal = $(modal);
  let $mData = $('#mData');

  $.ajax('/auth/login/?next=/', {
    type: 'GET',
    dataType: 'html',
    error(jqXHR, textStatus, errorThrown) {
      return $('body').prepend(`AJAX Error: ${textStatus}`);
      },
    success(data, textStatus, jqXHR) {
      $mData.html(data);
      return $modal.foundation('open');
     }
    });

}

window.chkFrm = function(form) {
  let this_loc = location.pathname;
  if (form.id === "select-single-year") {
    if (form.yr.value === "") {
      alert("Selet a year to filter on.");
      return false;
      } else if (form.yr.value.length !== 4) {
      alert("enter valid year (yyyy)");
      return false;
      } else {
      var new_loc = `/${form.yr.value}${tags ? tags : ""}`;
      }
    }

  location.href = new_loc;
  return false;
  }

console.log("index.js initialized ...");
