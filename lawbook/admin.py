from django.contrib import admin
from .models import Devotee, Filter, Resolution, Category, Section


class DevoteeAdmin(admin.ModelAdmin):

  ordering = ('name',)
  list_display = ['id', 'name', 'gbcmember', 'guru', 'sannyasi']
  list_display_links = ['name', ]


admin.site.register(Devotee, DevoteeAdmin)

class FilterAdmin(admin.ModelAdmin):

  ordering = ('order',)
  list_display = ['order', 'name', 'get_tags']
  list_display_links = ['name', ]

  def get_queryset(self, request):
    ### is prefetch_related working? ###
    return super(FilterAdmin, self).get_queryset(request).prefetch_related('tags')

  def get_tags(self, obj):
    ### doesn't seem to actually be returning anything... ###
    return u", ".join(t.name for t in obj.tags.all().order_by('slug'))

  get_tags.short_description = 'Tags'


admin.site.register(Filter, FilterAdmin)


class ResolutionAdmin(admin.ModelAdmin):
  """
  Admin class for Resolution
  """

  ordering = ('-year',)

  list_display = ['id', 'res_id', 'section', 'year', 'sort_order',
                  'category', 'published',
                  'title', 'country', 'get_countries', 'get_tags', 'get_devotees', ]
  list_filter = ['year', 'section', 'category', ]
  list_display_links = ['id', 'res_id', 'title', ]
  search_fields = ['title', 'resolution', 'tags', ]
  readonly_fields = ['res_id', ]

  def get_queryset(self, request):
    """ is prefetch_related working? """
    return super(ResolutionAdmin, self).get_queryset(request).prefetch_related('tags')

  def get_tags(self, obj):
    return u", ".join(t.name for t in obj.tags.all().order_by('slug'))
  get_tags.short_description = 'Tags'

  def get_countries(self, obj):
    strCountries = ""
    for c in obj.countries:
      strCountries += u"%s (%s), " % (c.name, c.code)
    #return u"%s (%s), ".join(c.name, c.code for c in obj.countries)
    return strCountries
  get_countries.short_description = 'Countries'

  def get_devotees(self, obj):
    return "\n".join([d.name for d in obj.devotees.all()])
  get_devotees.short_description = 'Devotees'

  fieldsets = (
    ('Resolutions',
      {'fields':
        (
         'res_id', 'section', 'year', 'sort_order', 'title', 'category', 'country', 'countries', 'tags', 'devotees',
         'published', 'presentation', 'resolution', 'comments_gbc', 'comments_deputy'
        )
      }
    ),
  )

  class Media:
    """
    This is not actually a class
    """
    js = [
        '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
        '/static/grappelli/tinymce_setup/tinymce_setup.js',
    ]


admin.site.register(Resolution, ResolutionAdmin)


class SectionAdmin(admin.ModelAdmin):
  """
  Admin class for sections.
  """

  ordering = ('section',)

  list_display = ['section', 'title', ]
  list_filter = ['section', ]
  list_display_links = ['section', 'title', ]
  search_fields = ['section', 'title', ]

  actions = []


admin.site.register(Section, SectionAdmin)


class CategoryAdmin(admin.ModelAdmin):
  """
  Admin class for categories
  """

  ordering = ('group', 'name',)

  list_display = ['group', 'name', 'description']
  list_filter = ['group', ]
  list_display_links = ['group', 'name', ]
  search_fields = ['group', 'name']

  actions = []

  fieldsets = (
      ('Category', {'fields': ('group', 'name', 'description',)}),
  )


admin.site.register(Category, CategoryAdmin)
