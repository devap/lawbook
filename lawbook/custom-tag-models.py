from taggit.managers import TaggableManager
from taggit.models import TagBase, GenericTaggedItemBase


class CustomTag(TagBase):
  name = models.CharField(max_length=50, unique=True)
  gbc = models.BooleanField()

    # ... fields here

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")

    # ... methods (if any) here


class Tagged(GenericTaggedItemBase):
    # TaggedWhatever can also extend TaggedItemBase or a combination of
    # both TaggedItemBase and GenericTaggedItemBase. GenericTaggedItemBase
    # allows using the same tag for different kinds of objects, in this
    # example Food and Drink.

    # Here is where you provide your custom Tag class.
    tag = models.ForeignKey(FilterTag,
                            related_name="%(app_label)s_%(class)s_items")


class Resolution(models.Model):
    # ... fields here

    tags = TaggableManager(through=TaggedWhatever)


class Filter(models.Model):
    # ... fields here

    tags = TaggableManager(through=TaggedWhatever)
