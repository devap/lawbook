from django.core.management.base import BaseCommand, CommandError
from lawbook.models import Resolution

from topia.termextract import extract
extractor = extract.TermExtractor()

class Command(BaseCommand):
  help = 'adds tags pulled from content'

  def handle(self, *args, **options):

  resolutions = Resolution.objects.all()
  tag_count = 0
  r_count = 0

  for r in resolutions:
    r_count += 1
    print "r.id: %s, r.title: %s" % (r.id, r.title)

    terms = extractor(r.resolution)
    #print "%s" % sorted(extractor(r.resolution))
    for term in terms:
      print "adding tag: %s to %s..." % (term[0], r.title)
      tag_count += 1
      r.tags.add(term[0])
    r.save()

    print "Done - added %s tags to %s resolutions." % (tag_count, r_count)
