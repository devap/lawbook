from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist
from collections import defaultdict
from taggit.models import Tag, TaggedItem
from lawbook.models import Resolution
from lawbook.new_resolution import NewResolution


class Command(BaseCommand):
  help = 'renumber pk'

  def handle(self, *args, **options):
    self.stdout.write('rebuilding resolutions...')
    for old_res in Resolution.objects.all():
      new_res = NewResolution()
      new_res.title = old_res.title
      new_res.published = old_res.published
      new_res.year = old_res.year
      new_res.time = old_res.time
      new_res.resolution = old_res.resolution
      new_res.presentation = old_res. presentation
      new_res.comments_gbc = old_res.comments_gbc
      new_res.comments_deputy = old_res.comments_deputy
      new_res.category = old_res.category
      new_res.section = old_res.section
      new_res.sort_order = old_res.sort_order

      new_res.save()
      nid = new_res.id
      oid = old_res.id
      tags = TaggedItem.objects.filter(object_id=oid)
      for tag in tags:
        tag.object_id = nid
        tag.save()
