from django.core.management.base import BaseCommand, CommandError
from collections import defaultdict, Counter
from lawbook.models import Resolution

class Command(BaseCommand):
  help = 'lists all tags and tracks the frequency of usage'

  def handle(self, *args, **options):

	tag_frequency = defaultdict(int)

	self.stdout.write('counting tags...')
	for item in Resolution.objects.all():
    for tag in item.tags.all():
      #self.stdout.write('name:' + tag.name + ', slug: ' + tag.slug)
      tag_frequency[tag.slug] += 1


	for slug in sorted(tag_frequency):
		self.stdout.write('slug: ' + slug + ': ' + str(tag_frequency[slug]))

	#Counter(tag_frequency).most_common()
	#self.stdout.write('Counter: ' + str(Counter))
