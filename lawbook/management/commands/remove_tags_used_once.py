from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist
from collections import defaultdict
from taggit.models import Tag, TaggedItem
from lawbook.models import Resolution


class Command(BaseCommand):
  help = 'removes tags only appearing once'

  def handle(self, *args, **options):

    tag_frequency = defaultdict(int)

    self.stdout.write('counting tags...')
    for item in Resolution.objects.all():
      for tag in item.tags.all():
        #self.stdout.write('name:' + tag.name + ', slug: ' + tag.slug)
        tag_frequency[tag.slug] += 1

    for slug in tag_frequency:
      self.stdout.write('tag_freqy[]: ' + str(tag_frequency[slug]))
      if tag_frequency[slug] == 1:
        self.stdout.write('slug: ' + slug + ', tag_frequency[]: ' + str(tag_frequency[slug]))

        try:
          tag = Tag.objects.get(slug=slug)
          items = TaggedItem.objects.filter(tag=tag)
        except ObjectDoesNotExist:
          raise CommandError('Slug "%s" does not exist' % slug)

        for item in items:
          obj = item.content_object
          self.stdout.write('deleting item[tag]: ' + obj.title + '[' + tag.slug + ']')

          obj.tags.remove(tag)

        self.stdout.write('deleting tag: ' + str(tag))
        tag.delete()
