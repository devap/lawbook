import datetime

from django.core.urlresolvers import reverse
from django.contrib.auth.models import User, Group
from django.db import models
from django.db.models import signals
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from django_countries.fields import CountryField
#from taggit.managers import TaggableManager
from taggit_autosuggest.managers import TaggableManager

from tastypie.models import create_api_key
signals.post_save.connect(create_api_key, sender=User)


"""
Code Styling:
  All database fields
  Custom manager attributes
  class Meta
  def __unicode__()
  def __str__()
  def save()
  def get_absolute_url()
  Any custom methods
"""


YEAR_CHOICES = []
for r in range(1975, (datetime.datetime.now().year + 2)):
  YEAR_CHOICES.append((r, r))


class Filter(models.Model):
  order = models.IntegerField(_('list order'))
  name = models.CharField(max_length=50, unique=True)

  tags = TaggableManager(blank=True, help_text=_("Separate each tag with a comma"))

  class Meta:
    ordering = ("order", )
    verbose_name = _("Filter")
    verbose_name_plural = _("Filters")

  def __unicode__(self):
    return self.name


class Devotee(models.Model):

  name = models.CharField(max_length=155)
  gbcmember = models.BooleanField(_("GBC Member"), default=False)
  guru = models.BooleanField(_("Initiating spiritual master"), default=False)
  sannyasi = models.BooleanField(_("Sannyasi"), default=False)

  def __unicode__(self):
    return self.name

  class Meta:
    ordering = ('name',)

class Section(models.Model):
  """
  Lawbook Sections
  """

  section = models.DecimalField(max_digits=6, decimal_places=1)
  title = models.CharField(max_length=255, blank=True, null=True)

  # The default manager.
  objects = models.Manager()

  class Meta:
    ordering = ("section",)
    verbose_name = _("Section")
    verbose_name_plural = _("Sections")

  def __unicode__(self):
    return "%s: %s" % (self.section, self.title)

  def get_absolute_url(self):
    return reverse('section_list_view', args=[self.number])


class Category(models.Model):
  """
  Lawbook Categories
  """

  group = models.CharField(max_length=55)
  name = models.CharField(max_length=255)
  description = models.TextField(blank=True, null=True)

  # The default manager.
  objects = models.Manager()

  class Meta:
    ordering = ("group",)
    verbose_name = _("Category")
    verbose_name_plural = _("Categories")

  def __unicode__(self):
    return "%s: %s" % (self.group, self.name)

  def get_absolute_url(self):
    return reverse('category_list_view', args=[self.id])


class Resolution(models.Model):
  """
  Main resolution item
  """

  published = models.BooleanField(default=False)
  year = models.IntegerField(_('year'),
                             choices=YEAR_CHOICES,
                             default=datetime.datetime.now().year
                             )

  sort_order = models.IntegerField(_('sort_order'),
                                   default=0,
                                   help_text=_("Determines order of listing within each year")
                                   )

  res_id = models.CharField(_('resolution #'),
                            max_length=55,
                            default="0",
                            blank=True,
                            null=True
                            )

  time = models.TimeField(null=True,
                          blank=True
                          )

  title = models.CharField(max_length=255,
                           help_text=_("Give the resolution a one-line summary"),
                           )

  resolution = models.TextField()

  section = models.ForeignKey(Section,
                              null=True,
                              blank=True,
                              default=0
                              )

  category = models.ForeignKey(Category,
                               null=True,
                               blank=True,
                               default=0
                               )

  """
  these should only be displayed with GBC login
  - presentation of proposal
  - GBC discussion/comments
  - deputy discussions/comments
"""
  presentation = models.TextField(null=True,
                                  blank=True,
                                  help_text=_("Presenatation of proposal")
                                  )

  comments_gbc = models.TextField(_("GBC Discussions/comments"),
                                  null=True,
                                  blank=True,
                                  help_text=_("GBC Discussions/comments")
                                  )

  comments_deputy = models.TextField(_("Deputy Discussions/comments"),
                                     null=True,
                                     blank=True,
                                     help_text=_("Deputy Discussions/comments")
                                     )

  tags = TaggableManager(blank=True,
                         help_text=_("Separate each tag with a comma")
                         )

  #filters = models.ManyToManyField(Filter)
  devotees = models.ManyToManyField(Devotee,
                                    blank=True
                                    )

  country = CountryField(blank=True,
                         default='IN',
                         countries_flag_url='//flags.example.com/{code}.gif',
                         blank_label='(select country)',
                         )

  countries = CountryField(blank=True,
                           default='IN',
                           countries_flag_url='//flags.example.com/{code}.gif',
                           multiple=True,
                           blank_label='(select country(ies))',
                           help_text='Hold down "Control on a PC", or "Command" on a Mac, to select more than one'
                           )

  class Meta:
    ordering = ("res_id",)
    verbose_name = _("Resolution")
    verbose_name_plural = _("Resolutions")

  def __unicode__(self):
    return "%s" % (self.id)

  def get_section(self):
    if self.section:
      return self.section.section
    else:
      return 000

  def get_absolute_url(self):
    return reverse('list', args=[self.id])






@receiver(signals.post_save, sender=Resolution)
def create_resolution_id(sender, instance, created, update_fields, **kwargs):
  if update_fields == {"res_id"}:
    #print "post_save not needed..."
    return
  else:
    #print "res_id being created: %s" % (instance.res_id)
    instance.res_id = "%04d-%03d-%03d" % (int(instance.year), int(instance.get_section()), int(instance.id))
    instance.save(update_fields={"res_id"})


@receiver(signals.post_save, sender=User)
def add_to_default_group(sender, **kwargs):
  """local function to add new users to the 'User' group."""
  user = kwargs["instance"]
  if kwargs["created"]:
    group = Group.objects.get(name='User')
    user.groups.add(group)
