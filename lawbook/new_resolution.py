import datetime

from django.core.urlresolvers import reverse
from django.contrib.auth.models import User, Group
from django.db import models
from django.db.models import signals
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

#from taggit.managers import TaggableManager
from taggit_autosuggest.managers import TaggableManager

from tastypie.models import create_api_key
signals.post_save.connect(create_api_key, sender=User)

from Lawbook.models import Section, Category, Resolution

YEAR_CHOICES = []
for r in range(1975, (datetime.datetime.now().year + 2)):
  YEAR_CHOICES.append((r, r))


class NewResolution(models.Model):
  """
  Main resolution item
  """

  published = models.BooleanField(default=False)
  year = models.IntegerField(_('year'),
                             choices=YEAR_CHOICES,
                             default=datetime.datetime.now().year
                             )

  sort_order = models.IntegerField(_('sort_order'),
                                   default=0,
                                   help_text=_("Determines order of listing within each year")
                                   )

  res_id = models.CharField(_('resolution #'),
                            max_length=55,
                            default="0",
                            blank=True,
                            null=True
                            )

  time = models.TimeField(null=True,
                          blank=True
                          )
  title = models.CharField(max_length=255,
                           help_text=_("Give the resolution a one-line summary"),
                           )

  resolution = models.TextField()

  section = models.ForeignKey(Section,
                              null=True,
                              blank=True,
                              default=0
                              )

  category = models.ForeignKey(Category,
                               null=True,
                               blank=True,
                               default=0
                               )

  """
  these should only be displayed with GBC login
  - presentation of proposal
  - GBC discussion/comments
  - deputy discussions/comments
"""
  presentation = models.TextField(null=True, blank=True, help_text=_("Presenatation of proposal"))
  comments_gbc = models.TextField("GBC Discussions/comments", null=True, blank=True, help_text=_("GBC Discussions/comments"))
  comments_deputy = models.TextField("Deputy Discussions/comments", null=True, blank=True, help_text=_("Deputy Discussions/comments"))

  tags = TaggableManager(blank=True,
                         help_text=_("Separate each tag with a comma, Surround phrase with quotes for multiple word tags.")
                         )

  class Meta:
    ordering = ("res_id",)
    verbose_name = _("Resolution")
    verbose_name_plural = _("Resolutions")

  def __unicode__(self):
    return "%s" % (self.id)

  def get_section(self):
    if self.section:
      return self.section.section
    else:
      return 000

  def get_absolute_url(self):
    return reverse('list', args=[self.id])


@receiver(signals.post_save, sender=NewResolution)
def create_resolution_id(sender, instance, created, update_fields, **kwargs):
  if update_fields == {"res_id"}:
    #print "post_save not needed..."
    return
  else:
    #print "res_id being created: %s" % (instance.res_id)
    instance.res_id = "%04d-%03d-%03d" % (int(instance.year), int(instance.get_section()), int(instance.id))
    instance.save(update_fields={"res_id"})
