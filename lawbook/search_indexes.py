from haystack import indexes
from .models import Resolution

"""
Simply run ./manage.py rebuild_index. You'll get some totals of how many models were processed and placed in the index.

Note
Using the standard SearchIndex, your search index content is only updated whenever you run either:
    ./manage.py update_index
or start afresh with
    ./manage.py rebuild_index

You should cron up a ./manage.py update_index job at whatever interval works best for your site
(using --age=<num_hours> reduces the number of things to update).

Alternatively, if you have low traffic and/or your search engine can handle it, the RealTimeSearchIndex automatically handles updates/deletes for you.

"""


class ResolutionIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    resolution = indexes.CharField(model_attr='resolution')
    year = indexes.DateTimeField(model_attr='year')

    def get_model(self):
        return Resolution

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()
        #return self.get_model().objects.all().order_by('year')
