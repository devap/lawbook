from django.contrib import admin
from django.conf.urls import include, url
from django.views.generic import TemplateView

from lawbook import views

admin.autodiscover()

lawbook_patterns = [

  url(r'^about/$', TemplateView.as_view(template_name='pages/about.html'), name='about'),

  url(
      regex=r'^$',
      view=views.ResolutionsListView.as_view(),
      name='home'
  ),

  url(
      regex=r'^category/',
      view=views.ResolutionsListView.as_view(),
      name='category'
  ),

  url(
      regex=r'^section/',
      view=views.ResolutionsListView.as_view(),
      name='section'
  ),

  url(
      regex=r'^title/',
      view=views.ResolutionsListView.as_view(),
      name='title'
  ),

  url(
      regex=r'^(?P<year1>\d{4})(?P<year2>\/\d{4})?/tags/(?P<tags>\w+.*)/$',
      view=views.ResolutionsListView.as_view(),
      name='years-tags'
  ),

  url(
      regex=r'^(?P<year1>\d{4})(?P<year2>\/\d{4})?/tag/(?P<tag>[-|\w|\d]+)/$',
      view=views.ResolutionsListView.as_view(),
      name='years-tagged'
  ),

  url(
      # regex=r'^(?P<year1>\d{4})/.*$',
      regex=r'^(?P<year1>\d{4})(?P<year2>\/\d{4})?/.*$',
      view=views.ResolutionsListView.as_view(),
      name='years'
  ),


  url(
      regex=r'^resolution/(?P<pk>\d+)/$',
      view=views.ResolutionsDetailView.as_view(),
      name='detail'
  ),

  # haystack
  # url(r'^search/$', include('haystack.urls')),
  url(
      regex=r'^search/$',
      view=views.CustomSearchView.as_view(),
      name='haystack_search'
  ),

]

