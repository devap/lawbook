#from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

#from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.mixins import LoginRequiredMixin

#from django.utils.decorators import method_decorator

#from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
#from django.views.decorators.debug import sensitive_post_parameters

# Haystack
#from haystack.query import EmptySearchQuerySet
from haystack.query import SearchQuerySet

#from haystack.inputs import AutoQuery, Exact, Clean
from haystack.generic_views import SearchView

from taggit.models import Tag
from .models import Resolution

# RESULTS_PER_PAGE = getattr(settings, '  ', 20)
RESULTS_PER_PAGE = 25


class ResolutionsListView(LoginRequiredMixin, ListView):
  model = Resolution
  ordering = 'year'
  context_object_name = "resolutions"
  tag = None
  tags = None
  year1 = 0
  year2 = 0
  date_range = False
  results_per_page = RESULTS_PER_PAGE

#required
  permissions = {
      "all": ("lawbook.resolution.chage_resolution", "taggit.tag.change_tag"),
  }

  def get_queryset(self, *args, **kwargs):
    if self.kwargs:
      self.tag = self.kwargs.get('tag')
      self.tags = self.kwargs.get('tags')
      if self.kwargs.get('year1'):
        self.year1 = self.kwargs.get('year1')
      if self.kwargs.get('year2'):
        self.year2 = self.kwargs.get('year2').replace("/", "")
      else:
        self.year2 = self.year1
      if self.year2 > self.year1:
        self.date_range = True

    if self.request.user.groups.filter(name='GBC').count():
      resolutions = Resolution.objects.all()
    else:
      resolutions = Resolution.objects.filter(published=True)

    if self.date_range:
      #resolutions = Resolution.objects.filter(year__gte=self.year1).filter(year__lte=self.year2)
      resolutions = resolutions.filter(year__gte=self.year1).filter(year__lte=self.year2)
    elif self.year1:
      #resolutions = Resolution.objects.filter(year=self.year1)
      resolutions = resolutions.filter(year=self.year1)
    #else:
    #  resolutions = Resolution.objects.all()

    return resolutions.order_by('year')

  def get_context_data(self, *args, **kwargs):
    context = super(ResolutionsListView, self).get_context_data(**kwargs)
    #context['user'] = self.request.user

    if self.tag:
      self.resolutions = self.resolutions.filter.prefetch_related(tags__slug__contains=self.tag)
      context['tag'] = Tag.objects.get(slug=self.tag)
    elif self.tags:
      self.tags = self.tags.split("/")
      self.resolutions = self.resolutions.filter.prefetch_related(tags__slug__in=self.tags)
      context['tags'] = Tag.objects.filter(slug__in=self.tags)

    context['year1'] = self.year1
    context['year2'] = self.year2
    context['date_range'] = self.date_range
    #context['resolutions_total_count'] = context['resolutions'].count()
    context['resolutions_total_count'] = len(context['resolutions'])

    """
    pagination
    """
    #paginator = Paginator(self.resolutions, self.results_per_page)
    paginator = Paginator(self.get_queryset(), self.results_per_page)
    page_range = paginator.page_range
    page = self.request.GET.get('page')

    try:
      resolutions_paginated = paginator.page(page)
    except PageNotAnInteger:
      # If page is not an integer, deliver first page.
      resolutions_paginated = paginator.page(1)
    except EmptyPage:
      # If page is out of range (e.g. 9999), deliver last page of results.
      resolutions_paginated = paginator.page(paginator.num_pages)

    range_begin = list(page_range)[:4]
    range_end = list(page_range)[-3:]

    context['page'] = page
    context['paginator'] = paginator
    context['page_range'] = page_range
    context['range_begin'] = range_begin
    context['range_end'] = range_end
    context['is_paginated'] = True
    context['resolutions_paginated'] = resolutions_paginated
    context['resolutions_paginated_count'] = len(context['resolutions_paginated'])

    """
    tags
    """
    self.common_tags = Resolution.tags.most_common()[:10]
    context['most_popular_tags'] = self.common_tags
    common_tag_ids = []
    for tag in self.common_tags:
      common_tag_ids.append(tag.id)
    #context['all_tags'] = Resolution.tags.all().exclude(id__in=common_tag_ids)
    context['all_tags'] = Resolution.tags.all().order_by("name")

    return context

  def get_template_names(self):
    template = "resolution_list_react"
    #if self.year1 and not self.date_range:
    #  template = "resolution_list_years"

    return ['lawbook/%s.html' % template]


class ResolutionsDetailView(DetailView):
  model = Resolution
  context_object_name = "resolution"




class CustomSearchView(SearchView):
  """
  Search view
  """
  __name__ = 'SearchView'
  template = 'search/search.html'
  results = SearchQuerySet()
  results_per_page = RESULTS_PER_PAGE

  def get_queryset(self):
    queryset = super(CustomSearchView, self).get_queryset()
    return queryset.order_by('-year')
    # further filter queryset based on some set of criteria
    # #return queryset.filter(pub_date__gte=date(2015, 1, 1))

  def get_context_data(self, *args, **kwargs):
    """
    Allows the addition of more context variables as needed.
    Must return a dictionary.
    """
    context = super(CustomSearchView, self).get_context_data(*args, **kwargs)

    paginator = Paginator(self.get_queryset(), self.results_per_page)

    page = self.request.GET.get('page')
    try:
      contacts = paginator.page(page)
    except PageNotAnInteger:
     # If page is not an integer, deliver first page.
      contacts = paginator.page(1)
    except EmptyPage:
     # If page is out of range (e.g. 9999), deliver last page of results.
     contacts = paginator.page(paginator.num_pages)

    # create a range for displaying prev/next links with range in between
    page_range = paginator.page_range
    range_begin = list(page_range)[:4]
    range_end = list(page_range)[-3:]
    #range_begin = paginator.page_range[:4]
    #range_end = paginator.page_range[-3:]

    self.common_tags = Resolution.prefectch_related.tags.most_common()[:10]
    context['most_popular_tags'] = self.common_tags
    common_tag_ids = []
    for tag in self.common_tags:
      common_tag_ids.append(tag.id)
    #context['all_tags'] = Resolution.tags.all().exclude(id__in=common_tag_ids)
    context['all_tags'] = Resolution.prefetch_related.tags.all().order_by("name")

    context.update({
                   'contacts': contacts,
                   'page': page,
                   'search_page': True,
                   'paginator': paginator,
                   'page_range': page_range,
                   'range_begin': range_begin,
                   'range_end': range_end,
                   'request': self.request,
                   'all_tags': Resolution.prefetch_related.tags.all().order_by("name"),
                   'most_popular_tags': Resolution.prefetch_related.tags.most_common()[:10]
                   })

    return context
