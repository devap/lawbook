# -*- coding: utf-8 -*-
# services/api.py

#from django.contrib.auth import authenticate, login, logout

from tastypie.authentication import MultiAuthentication, ApiKeyAuthentication, SessionAuthentication
#from tastypie.authorization import DjangoAuthorization, ReadOnlyAuthorization
from tastypie.authorization import ReadOnlyAuthorization
from tastypie import fields
from tastypie.resources import ModelResource, ALL_WITH_RELATIONS
#from tastypie.serializers import Serializer

from taggit.models import Tag

from lawbook.models import Filter, Resolution, Category, Section


class TagResource(ModelResource):
  """Return list of all tags, with the 10 most common tags at the beginning."""

  def alter_list_data_to_serialize(self, request, data):
    """Append the 10 most common tags to the beginning of the full list."""
    common_tags = []
    for tag in Resolution.tags.most_common()[:10]:
      common_tags.append({
        'id': tag.id,
        'slug': tag.slug,
        'name': tag.name
      })
    data['meta']['most_used'] = common_tags

    return data

  class Meta:
    """Main configuration section."""

    queryset = Tag.objects.all().order_by('slug')
    # filtering = { 'slug': ALL, }
    filtering = {'slug': ALL_WITH_RELATIONS, }
    resource_name = 'tags'
    limit = 0
    #authentication = MultiAuthentication(ApiKeyAuthentication(), SessionAuthentication())
    authentication = MultiAuthentication(ApiKeyAuthentication(), SessionAuthentication())
    authorization = ReadOnlyAuthorization()


class FilterResource(ModelResource):
  """Provides a list of filters."""
  tags = fields.ToManyField(TagResource, 'tags', full=True)

  class Meta:
    """Main configuration section."""

    queryset = Filter.objects.all().order_by('order')
    resource_name = 'filters'
    limit = 0
    authentication = MultiAuthentication(ApiKeyAuthentication(), SessionAuthentication())
    authorization = ReadOnlyAuthorization()


class ResolutionResource(ModelResource):
  """This return a full list of all resolutions, including tags."""

  tags = fields.ToManyField(TagResource, 'tags', full=True)
  section = fields.CharField(attribute='section', null=True)
  category = fields.CharField(attribute='category', null=True)

  class Meta:
    """This is required, and provides the main configuration of the api."""

    queryset = Resolution.objects.all().order_by('-res_id')
    filtering = dict(tags=ALL_WITH_RELATIONS)
    # full = True
    # filtering = dict(tags = ALL)
    resource_name = 'r'
    limit = 0
    allowed_methods = ['get']
    authentication = MultiAuthentication(ApiKeyAuthentication(), SessionAuthentication())
    authorization = ReadOnlyAuthorization()

  def alter_list_data_to_serialize(self, request, data):
    """Include additional data with results."""
    #GROUPS = "%s" % request.user.groups.all()
    GROUPS = "%s" % (request.user.groups.values_list('name', flat=True))
    #GROUPS = request.user.groups.values_list('id', flat=True)
    user = {
      'username': request.user,
      'name': request.user.first_name,
      'groups': GROUPS,
      'api_key': request.user.api_key.key,
      'is_authenticated': request.user.is_authenticated(),
      'is_superuser': request.user.is_superuser
    }

    data['meta']['user'] = user
    #data['meta']['user']['name'] = request.user.first_name
    #data['meta']['user']['group'] = request.user.group

    return data

  def dehydrate(self, bundle):
    """get the section ID and add it to the [data][objects] bunvdle."""
    bundle.data['section_id'] = bundle.obj.get_section()
    return bundle


class CategoryResource(ModelResource):
  """Provides a list of categories."""

  class Meta:
    """Main configuration section."""

    queryset = Category.objects.all()
    resource_name = 'c'
    limit = 0


class SectionResource(ModelResource):
  """Provide a list of sections."""

  class Meta:
    """Main configuration section."""

    queryset = Section.objects.all()
    resource_name = 's'
    limit = 0
